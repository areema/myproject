from django.contrib import admin
from .models import Post

# Register your models here.
# admin.site.register(Post)
class PostAdmin(admin.ModelAdmin):
 list_filter = ('created',)
 search_fields = ('title', 'body')
 ordering = ['title']

admin.site.register(Post, PostAdmin)